import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import axios from 'axios';
import { host } from '../configs';
import { List, Avatar } from 'antd';

const listData = [];
    for(let i = 0; i < 23; i++) {
    listData.push({
        href: 'http://ant.design',
        name: `ant design part ${i}`,
        image: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        info: 'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
    });
}

const IconText = ({ type, text }) => (
    <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
    </span>
);

class Gestore extends Component {
    state = {
        monuments: []
    }

componentDidMount() {
    this.setState(this.props.location.state);
    this.fetchMonuments();
}

fetchMonuments() {
    axios({
        method: 'GET',
        url: `${host}/poi/${this.props.location.state.userName}`,
        mode: 'no-cors',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
      })
        .then(result => {
          result.data.forEach(element => {
              this.setState({monuments: [...this.state.monuments, element]});
          });
        })
        .catch(this.setState({falseCredentials: true}));
}



render() {
    return (
        <div className="gestione">
            <h1>Hello {this.state.userName}</h1>
            <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: (page) => {
                console.log(page);
            },
            pageSize: 3,
            }}
            dataSource={this.state.monuments}
            footer={<div><b>ant design</b> footer part</div>}
            renderItem={item => (
            <List.Item
                key={item.name}
                actions={[<IconText type="star-o" text="156" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
                extra={<img width={272} alt="logo" src={item.image} />}
            >
                <List.Item.Meta
                image={<Avatar src={item.image} />}
                name={<a href={item.href}>{item.name}</a>}
                description={item.description}
                />
                {item.info}
            </List.Item>
            )}
            />
        </div>
    )
}
}

export default Gestore;